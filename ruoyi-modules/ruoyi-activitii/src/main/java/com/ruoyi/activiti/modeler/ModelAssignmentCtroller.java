package com.ruoyi.activiti.modeler;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.api.domain.SysRole;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.service.ISysRoleService;
import com.ruoyi.system.service.ISysUserService;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 流程模型指派代理，组
 *
 * @author 小蜜獾
 */
@RestController
@AllArgsConstructor
@RequestMapping("/activiti/assignment")

public class ModelAssignmentCtroller extends BaseController {
//    @Autowired
//    private ISysUserService userService;
//    @Autowired
//    private ISysRoleService roleService;
//
//    /**
//     * 获取用户数据
//     */
////    @PreAuthorize("@ss.hasPermi('activiti:assignment:user:list')")
//    @PostMapping("/user/list")
//    public TableDataInfo list(SysUser user,String filterText) {
//
//        startPage();
//        user.setUserName(filterText);
////        user.setNickName(filterText);
////        if(StringUtils.isNotEmpty(filterText) && filterText.matches("\\d+")){
////            Long s= Long.valueOf(filterText);
////            user.setDeptId(s);
////        }
//
//        List<SysUser> list = userService.selectUserList(user);
//        return getDataTable(list);
//
//    }
//    @PostMapping("/group/list")
//    public TableDataInfo list(SysRole role,String filterText)
//    {
//        startPage();
////        role.setRoleKey(filterText);
//        role.setRoleName(filterText);
//        List<SysRole> list = roleService.selectRoleList(role);
//        return getDataTable(list);
//    }
}
